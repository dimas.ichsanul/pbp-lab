import 'package:flutter/material.dart';

import './models/project.dart';
import 'models/project.dart';

const DUMMY_PROJECT = const [
  Project(
    id: 'c1',
    namaProject: 'Data Science',
    bayaran: 100000,
    deskripsi: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    estimasi: '10 hari',
  ),
  Project(
    id: 'c2',
    namaProject: 'Kacamata Transparan',
    bayaran: 100000,
    deskripsi: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    estimasi: '10 hari',
  ),
  Project(
    id: 'c3',
    namaProject: 'Peduli Miskin',
    bayaran: 100000,
    deskripsi: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
    estimasi: '10 hari',
  ),
];