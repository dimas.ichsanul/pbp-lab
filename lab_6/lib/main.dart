import 'package:flutter/material.dart';

import './dummy_data.dart';
import './screens/tabs_screen.dart';
import './screens/home_screen.dart';
import './models/project.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };
  List<Project> _availableMeals = DUMMY_PROJECT;
  List<Project> _favoriteProjects = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;

      _availableMeals = DUMMY_PROJECT.where((meal) {
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String projectId) {
    final existingIndex =
        _favoriteProjects.indexWhere((project) => project.id == projectId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteProjects.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteProjects.add(
          DUMMY_PROJECT.firstWhere((project) => project.id == projectId),
        );
      });
    }
  }

  bool _isProjectFavorite(String id) {
    return _favoriteProjects.any((project) => project.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Project Channel',
      theme: ThemeData(
        canvasColor: Color(0xFFdfe6e9),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color(0xFFb2bec3),
            ),
            bodyText2: TextStyle(
              color: Color(0xFFb2bec3),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => TabsScreen(_favoriteProjects),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => HomeScreen(),
        );
      },
    );
  }
}
