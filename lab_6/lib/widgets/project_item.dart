import 'package:flutter/material.dart';


class ProjectItem extends StatelessWidget {
  final String id;
  final String namaProject;
  final int bayaran;
  final String deskripsi;
  final String estimasi;

  ProjectItem(this.id, this.namaProject, this.bayaran, this.deskripsi, this.estimasi);

  void selectProject(BuildContext ctx) {

  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectProject(context),
      splashColor: Color(0xFFb2bec3),
      borderRadius: BorderRadius.circular(15),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text('$namaProject'),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.attach_money_rounded,
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text('$bayaran'),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.work,
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Text('$estimasi'),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Flexible(
                child: new Container(
                  padding: new EdgeInsets.only(right: 13.0),
                  child: new Text(
                    '$deskripsi',
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
