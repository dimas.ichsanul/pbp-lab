import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
          color: Color(0xFF747d8c),
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color(0xFFdfe4ea),
            child: Text(
              'Daftar Halaman',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Color(0xFF747d8c)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('FAQ', Icons.question_answer, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Buat Proyek', Icons.file_copy, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Proyek Saya', Icons.star_rounded, () {
            Navigator.of(context).pushReplacementNamed('/');
          })
        ],
      ),
    );
  }
}
