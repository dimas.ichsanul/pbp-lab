import 'package:flutter/foundation.dart';

class Project {
  final String id;
  final String namaProject;
  final int bayaran;
  final String deskripsi;
  final String estimasi;

  const Project({
    @required this.id,
    @required this.namaProject,
    @required this.bayaran,
    @required this.deskripsi,
    @required this.estimasi,
  });
}
