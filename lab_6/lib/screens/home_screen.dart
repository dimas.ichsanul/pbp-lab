import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/project_item.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      // padding: const EdgeInsets.all(25),
      children: DUMMY_PROJECT
          .map(
            (catData) => ProjectItem(
          catData.id,
          catData.namaProject,
          catData.bayaran,
          catData.deskripsi,
          catData.estimasi,
        ),
      ).toList(),
          // .toList(),
      // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
      //   maxCrossAxisExtent: 200,
      //   // childAspectRatio: 3 / 2,
      //   crossAxisSpacing: 20,
      //   mainAxisSpacing: 20,
      );
    // );
  }
}
