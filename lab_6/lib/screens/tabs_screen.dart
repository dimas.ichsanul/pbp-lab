import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';
import './my_project_screen.dart';
import './home_screen.dart';
import '../models/project.dart';

class TabsScreen extends StatefulWidget {
  final List<Project> favoriteProjects;

  TabsScreen(this.favoriteProjects);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        'page': HomeScreen(),
        'title': 'Daftar Project',
      },
      {
        'page': MyProjectScreen(widget.favoriteProjects),
        'title': 'FAQ',
      },
      {
        'page': MyProjectScreen(widget.favoriteProjects),
        'title': 'Buat Proyek',
      },
      {
        'page': MyProjectScreen(widget.favoriteProjects),
        'title': 'Proyek Saya',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
        shadowColor: Color(0xFFb2bec3),
        backgroundColor: Color(0xFF2bec3),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Color(0xFFb2bec3),
        unselectedItemColor: Color(0xFF636e72),
        selectedItemColor: Color(0xFFf1f2f6),
        currentIndex: _selectedPageIndex,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Color(0xFFb2bec3),
            icon: Icon(Icons.file_copy_rounded),
            title: Text('Daftar Project'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Color(0xFFb2bec3),
            icon: Icon(Icons.question_answer_rounded),
            title: Text('FAQ'),
          ),
          BottomNavigationBarItem(
          backgroundColor: Color(0xFFb2bec3),
          icon: Icon(Icons.file_copy),
          title: Text('Buat Proyek'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Color(0xFFb2bec3),
            icon: Icon(Icons.star_rounded),
            title: Text('Proyek Saya'),
          )
        ],
      ),
    );
  }
}
