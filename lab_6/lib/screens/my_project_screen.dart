import 'package:flutter/material.dart';

import '../models/project.dart';
import '../widgets/project_item.dart';

class MyProjectScreen extends StatelessWidget {
  static const routeName = '/my-project';

  final List<Project> myProject;

  MyProjectScreen(this.myProject);

  @override
  Widget build(BuildContext context) {
    if (myProject.isEmpty) {
      return Center(
        child: Text('Anda belum membuat project apapun.'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return Center(
            child: Text('Anda belum membuat project apapun.'),
          );
        },
        itemCount: myProject.length,
      );
    }
  }
}
