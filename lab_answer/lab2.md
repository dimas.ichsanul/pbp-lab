# Nama : Dimas Ichsanul Arifin <br> NPM : 2006595791 <br> Kelas : PBP - C <br> Kode Asdos : ZH

## 1. Apakah perbedaan antara JSON dan XML?

| JSON | XML |
| ------------- | ------------- |
| Javascript Object Notation | extensible Markup Language |
| Didasarkan pada bahasa JavaScript | Berasal dari SGML (Standard Generalized Markup Language) |
| Format berupa *data interchange* | Format berupa *markup language* |
| Tidak memerlukan start dan end tag | Memerlukan start dan end tag |
| Digunakan untuk mengirim data melalui internet agar kemudian dapat diparse | Digunakan untuk menstrukturkan data agar dapat menganotasikan *metadata* dan memparsenya |
| Tidak terlalu berat dan lebih cepat | Relatif lebih berat dan lebih lambat |
| Menggunakan struktur data map | Menggunakan struktur data tree |
| Umunya digunakan untuk merepresentasikan objek | Umumnya digunakan untuk merepresentasikan dokuman |
| Cenderung digunakan untuk pengiriman data antara server dan browser | Cenderung digunakan untuk menyimpan informasi |
| Dapat diparse dengan fungsi JavaScript standar | Harus diparse dengan XML parser | 


## 2. Apakah perbedaan antara HTML dan XML?

| HTML | XML |
| ------------- | ------------- |
| Hyper Text Markup Language | extensible Markup Language |
| Merupakan *markup language* | Merupakan *markup language* standar yang menyediakan *framework* untuk mendefinisikan *markup language* lainnya |
| Bersifat statis | Bersifat dinamis |
| Tidak *case-sensitive* | *Case-sensitive* |
| HTML *tag* digunakan untuk menampilkan data | XML *tag* digunakan untuk mendeskripsikan data |
| Beberapa *tag* tidak memerlukan *closing tag* | Setiap *tag* memerlukan *closing tag* |
| Memiliki *tag* yang telah dibuat | Tidak memiliki *tag* yang telah dibuat |
| *White space* tidak disimpan | *White space* dapat disimpan |
| Digunakan untuk menampilkan data | Digunakan untuk mengirimkan dan data |


