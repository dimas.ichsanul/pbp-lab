from django.contrib import admin
from .models import Friend

#Register Friend model here
admin.site.register(Friend)
