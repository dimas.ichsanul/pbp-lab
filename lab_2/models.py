from django.db import models

# Create Note models
class Note(models.Model):
    sender = models.CharField(max_length=30)
    to = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=100)