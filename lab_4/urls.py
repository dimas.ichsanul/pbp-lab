from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index_lab4'),
    path('add-note/', add_note, name='tambah_catatan'),
    path('note-list/', note_list, name='list_catatan')
]
