import 'package:flutter/material.dart';

Color primaryColor = Color(0xFF314728);
Color greyColor = Color(0xFFA9B0A6);
Color orangeColor = Color(0xFFF9B650);
Color backgroundColor = Color(0xFFE5E5E5);
Color whiteColor = Color(0xFFFFFFFF);
