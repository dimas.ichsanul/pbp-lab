from django.urls import path
from .views import get_note, index

urlpatterns = [
    path('', index, name='index_lab5'),
    path('notes/<id>', get_note, name='notes_lab5'),
]