// pk digunakan buat identifier unik antara satu message dengan yang lain buat yang mau kita liat detailnya
$(document).ready(function () {
    $("tbody").on('click', '.view', function() {
      var pk = $(this).attr("value");
        $.ajax({
          url: `/lab-5/notes/${pk}`,
          success: function (result) {
            $("#modal").css("display", "flex");
            // kita isi card (view_modal.html) dengan field yang kita dapat dari data pada tabel
            $("#msg-title").text(result.fields.title);
            $("#msg-from").text(result.fields.sender);
            $("#msg-to").text(result.fields.to);
            $("#msg").text(result.fields.message);
          },
        });
      });
  });
  
  // pas close modal, kita set display ke none
  $(".close").click(() => {
    $("#modal").css("display", "none");
  });
  