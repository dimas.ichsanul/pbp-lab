$(document).ready(function () {
    $.ajax({
      url: "/lab-2/json",
      success: function (results) {
        results.map((result) => {
          $("tbody").append(`
            <tr>
                <td>${result.fields.sender}</td>
                <td>${result.fields.to}</td>
                <td>${result.fields.title}</td>
                <td>${result.fields.message}</td>
                <td>
                    <button class="btn btn-primary view" value="${result.pk}">View</button>
                    <button class="btn btn-warning" value="${result.pk}">Edit</button>
                    <button class="btn btn-danger" value="${result.pk}">Delete</button>
                </td>
            </tr>`);
        });
      },
    });
  });
  