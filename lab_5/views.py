from django.core import serializers
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render, HttpResponseRedirect, redirect
from lab_2.models import Note
from .forms import NoteForm
import json

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    context = {'notes': notes}
    return render(request, 'lab5_index.html', context)

def get_note(request, id):
    obj = Note.objects.get(id = id)
    data = serializers.serialize('json', [obj, ])

    struct = json.loads(data)
    data = json.dumps(struct[0])
    return HttpResponse(data, content_type='application/json')

def update_note(request, id):
    context = {}
    obj = get_object_or_404(Note, id = id)

    form = NoteForm(request.POST or None, instance=obj)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')

    context['form']= form
    return render(request, "lab4_form.html", context)

